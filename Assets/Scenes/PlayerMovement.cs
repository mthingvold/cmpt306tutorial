﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public enum MOVEMENT_TYPE { MANUAL, FORCE, VELOCITY}

    private Rigidbody2D rbody;
    public float manual;    //delta for manual movement speed
    public float force;     //ammount of force applie for movement
    public float velocity;  //speed of velocity-based movement

    public MOVEMENT_TYPE movement_type;
    //Awake is called before Start()
    void Awake()
    {

    }
    // Start is called before the first frame update
    void Start()
    {
        rbody = GetComponent<Rigidbody2D>();
        //rbody.AddForce(new Vector2(1, 1));
    }
    //FixedUpdate called once per physics step
    /*void FixedUpdate()
    {

    }
    */
    // Update is called once per frame
    void Update()
    {
        //Get player input
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");


        // Move the character
        switch(movement_type)
        {
            case MOVEMENT_TYPE.MANUAL:
                Vector2 pos = transform.position;
                transform.position = new Vector2(pos.x+manual*h, pos.y+manual*v);
                //transform.position = new Vector2(pos.x + manual, pos.y + manual);
                break;
            case MOVEMENT_TYPE.FORCE:
                rbody.AddForce(new Vector2(force*h, force*v));
                break;
            case MOVEMENT_TYPE.VELOCITY:
                rbody.velocity = new Vector2(velocity*h, velocity*v);
                break;
        }
    }
    /*
    //Called at the end of a frame
    void LateUpdate()
    {

    }
    */
}
